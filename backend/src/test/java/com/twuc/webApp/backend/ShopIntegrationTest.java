package com.twuc.webApp.backend;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.backend.contract.CreateItemRequest;
import com.twuc.webApp.backend.contract.CreateOrderItem;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class ShopIntegrationTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void should_create_item_when_post_request() throws Exception {
        mockMvc.perform(createItem("cola", 5.5, "cup",
                "https://www.hingwongmarket.ie/wp-content/uploads/2017/12/391.Coca-Cola-330ml-%E5%8F%AF%E4%B9%90.jpg"))
                .andExpect(status().is(201));
    }

    @Test
    void should_return_error_message_when_create_same_item() throws Exception {
        mockMvc.perform(createItem("cola", 5.5, "cup",
                "https://www.hingwongmarket.ie/wp-content/uploads/2017/12/391.Coca-Cola-330ml-%E5%8F%AF%E4%B9%90.jpg"));
        mockMvc.perform(createItem("cola", 5.5, "cup",
                "https://www.hingwongmarket.ie/wp-content/uploads/2017/12/391.Coca-Cola-330ml-%E5%8F%AF%E4%B9%90.jpg"))
                .andExpect(status().is(400))
                .andExpect(jsonPath("$.message").value("The item exists"));
    }

    @Test
    void should_get_items_when_send_get_request() throws Exception {
        mockMvc.perform(createItem("cola", 5.5, "cup",
                "https://www.hingwongmarket.ie/wp-content/uploads/2017/12/391.Coca-Cola-330ml-%E5%8F%AF%E4%B9%90.jpg"));
        mockMvc.perform(get("/api/items"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].name").value("cola"));
    }

    private MockHttpServletRequestBuilder createItem(String name, double price, String unit, String imgUrl) throws JsonProcessingException {
        CreateItemRequest createItemRequest = new CreateItemRequest(name, price, unit, imgUrl);
        String json = objectMapper.writeValueAsString(createItemRequest);
        return post("/api/items")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json);
    }

    @Test
    void should_create_order_item_when_post_request() throws Exception {
        mockMvc.perform(createItem("cola", 5.5, "cup",
                "https://www.hingwongmarket.ie/wp-content/uploads/2017/12/391.Coca-Cola-330ml-%E5%8F%AF%E4%B9%90.jpg"));
        CreateOrderItem orderItem = new CreateOrderItem("cola", 5L);
        mockMvc.perform(post("/api/order-items")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(orderItem)))
                .andExpect(status().is(201));
    }

    @Test
    void should_get_order_items_when_send_request() throws Exception {
        mockMvc.perform(createItem("cola", 5.5, "cup",
                "https://www.hingwongmarket.ie/wp-content/uploads/2017/12/391.Coca-Cola-330ml-%E5%8F%AF%E4%B9%90.jpg"));
        CreateOrderItem orderItem = new CreateOrderItem("cola", 5L);
        mockMvc.perform(post("/api/order-items")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(orderItem)));
        mockMvc.perform(get("/api/order-items"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].quantity").value(5L))
                .andExpect(jsonPath("$[0].item.name").value("cola"));
    }

    @Test
    void should_add_quantity_when_send_same_item() throws Exception {
        mockMvc.perform(createItem("cola", 5.5, "cup",
                "https://www.hingwongmarket.ie/wp-content/uploads/2017/12/391.Coca-Cola-330ml-%E5%8F%AF%E4%B9%90.jpg"));
        CreateOrderItem orderItem = new CreateOrderItem("cola", 5L);
        mockMvc.perform(post("/api/order-items")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(orderItem)));
        mockMvc.perform(post("/api/order-items")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(orderItem)));
        mockMvc.perform(get("/api/order-items"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].quantity").value(10L))
                .andExpect(jsonPath("$[0].item.name").value("cola"));
    }

    @Test
    void should_delete_order_item_when_send_request() throws Exception {
        mockMvc.perform(createItem("cola", 5.5, "cup",
                "https://www.hingwongmarket.ie/wp-content/uploads/2017/12/391.Coca-Cola-330ml-%E5%8F%AF%E4%B9%90.jpg"));
        CreateOrderItem orderItem = new CreateOrderItem("cola", 5L);
        mockMvc.perform(post("/api/order-items")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(orderItem)));
        mockMvc.perform(delete("/api/order-items/1"))
                .andExpect(status().is(200));
    }
}

package com.twuc.webApp.backend.contract;

import javax.validation.constraints.NotNull;

public class CreateItemRequest {
    @NotNull
    private String name;
    @NotNull
    private double price;
    @NotNull
    private String unit;
    @NotNull
    private String imgUrl;

    public CreateItemRequest(String name, double price, String unit, String imgUrl) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.imgUrl = imgUrl;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImgUrl() {
        return imgUrl;
    }
}

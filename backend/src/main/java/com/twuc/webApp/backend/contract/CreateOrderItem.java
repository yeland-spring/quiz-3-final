package com.twuc.webApp.backend.contract;

import javax.validation.constraints.NotNull;

public class CreateOrderItem {
    @NotNull
    private String name;
    @NotNull
    private Long quantity;

    public CreateOrderItem(String name, Long quantity) {
        this.name = name;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public @NotNull Long getQuantity() {
        return quantity;
    }
}

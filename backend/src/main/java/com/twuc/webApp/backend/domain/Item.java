package com.twuc.webApp.backend.domain;

import com.twuc.webApp.backend.contract.CreateItemRequest;
import org.hibernate.criterion.Order;

import javax.persistence.*;

@Entity
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 32, unique = true)
    private String name;
    @Column(nullable = false)
    private double price;
    @Column(nullable = false, length = 32)
    private String unit;
    @Column(nullable = false)
    private String imgUrl;

    public Item() {
    }

    public Item(CreateItemRequest itemRequest) {
        this.name = itemRequest.getName();
        this.price = itemRequest.getPrice();
        this.unit = itemRequest.getUnit();
        this.imgUrl = itemRequest.getImgUrl();
    }

    public Item(String name, double price, String unit, String imgUrl) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.imgUrl = imgUrl;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImgUrl() {
        return imgUrl;
    }
}

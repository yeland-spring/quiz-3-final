package com.twuc.webApp.backend.web;

import com.twuc.webApp.backend.contract.CreateItemRequest;
import com.twuc.webApp.backend.contract.CreateOrderItem;
import com.twuc.webApp.backend.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class ShopController {
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private OrderItemRepository orderItemRepository;

    @PostMapping("/items")
    private ResponseEntity<ErrorMessage> createItem(@RequestBody @Valid CreateItemRequest itemRequest) {
        Item item = new Item(itemRequest);
        List<Item> items = itemRepository.findAllByName(item.getName());
        if (items.size() != 0) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorMessage("The item exists"));
        }
        itemRepository.save(item);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/items")
    private ResponseEntity<List<Item>> getItems() {
        List<Item> items = itemRepository.findAll();
        return ResponseEntity.ok().body(items);
    }

    @PostMapping("/order-items")
    private ResponseEntity<Void> createItem(@RequestBody @Valid CreateOrderItem createOrderItem) {
        List<OrderItem> orderItems = orderItemRepository.findAllByItem_name(createOrderItem.getName());
        List<Item> items = itemRepository.findAllByName(createOrderItem.getName());
        if(orderItems.size() == 0) {
            OrderItem orderItem = new OrderItem(createOrderItem.getQuantity());
            orderItem.setItem(items.get(0));
            orderItemRepository.save(orderItem);
        } else {
            OrderItem originOrderItem = orderItems.get(0);
            originOrderItem.setQuantity(originOrderItem.getQuantity() + createOrderItem.getQuantity());
            orderItemRepository.save(originOrderItem);
        }
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/order-items")
    private ResponseEntity<List<OrderItem>> getOrderItems() {
        List<OrderItem> orderItems = orderItemRepository.findAll();
        return ResponseEntity.ok().body(orderItems);
    }

    @DeleteMapping("/order-items/{id}")
    private ResponseEntity<Void> deleteOrderItems(@PathVariable Long id) {
        orderItemRepository.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

}

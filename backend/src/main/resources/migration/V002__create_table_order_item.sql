CREATE TABLE order_item (
    `id` BIGINT AUTO_INCREMENT PRIMARY KEY ,
    `quantity` BIGINT NOT NULL ,
    `item_id` BIGINT NOT NULL ,
    FOREIGN KEY (item_id) REFERENCES item(id)
) ENGINE = InnoDB, DEFAULT CHARSET = utf8mb4;
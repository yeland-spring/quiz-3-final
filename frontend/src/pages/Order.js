import React, {Component} from 'react';
import Header from "../components/Header";
import Footer from "../components/Footer";
import OrderProduct from "../components/OrderProduct";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {getOrderProducts} from "../actions/orderActions";
import {Link} from "react-router-dom";

class Order extends Component {
  constructor(props, context) {
    super(props, context);
  }

  componentDidMount() {
    this.props.getOrderProducts()
  }

  render() {
    const orderProducts = this.props.orderProducts;
    return (
      <div>
        <Header/>
        <form>
          {this.show(orderProducts)}
        </form>
        <Footer/>
      </div>
    )
  }

  show(orderProducts) {
    if (orderProducts.length === 0) {
      return <p>暂无订单，返回<Link to="/">商城页面</Link>继续购买</p>
    }
    return <table>
      <thead>
      <tr>
        <th>名字</th>
        <th>单价</th>
        <th>数量</th>
        <th>单位</th>
        <th>操作</th>
      </tr>
      </thead>
      <tbody>
      {orderProducts.map((orderProduct, index) =>
        <OrderProduct product={orderProduct} key={index}/>)}
      </tbody>
    </table>
  }
}

const mapStateToProps = state => ({
  orderProducts: state.orderReducer.orderProducts
});


const mapDispatchToProps = dispatch => bindActionCreators({
  getOrderProducts
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Order);

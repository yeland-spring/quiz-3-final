import React, {Component} from 'react';
import Header from "../components/Header";
import Footer from "../components/Footer";
import Input from "../components/Input";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {addProduct} from "../actions/productActions";

class AddProduct extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      name: '',
      price: '',
      unit: '',
      img: ''
    };
    this.setName = this.setName.bind(this);
    this.setPrice = this.setPrice.bind(this);
    this.setUnit = this.setUnit.bind(this);
    this.setImg = this.setImg.bind(this);
    this.setDisabled = this.setDisabled.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  render() {
    return (
      <div>
        <Header />
        <form>
          <h1>添加商品</h1>
          <Input name="名称" getInput={this.setName}/>
          <Input name="价格" getInput={this.setPrice}/>
          <Input name="单位" getInput={this.setUnit}/>
          <Input name="图片" getInput={this.setImg}/>
          <button disabled={this.setDisabled()} onClick={this.handleClick}>提交</button>
        </form>
        <Footer />
      </div>
    );
  }

  setName(name) {
    this.setState({
      name: name.trim()
    })
  }

  setPrice(price) {
    this.setState({
      price: price.trim()
    })
  }

  setUnit(unit) {
    this.setState({
      unit: unit.trim()
    })
  }

  setImg(img) {
    this.setState({
      img: img.trim()
    })
  }

  setDisabled() {
    let isNull = this.state.name === '' || '' === this.state.price || '' === this.state.unit || '' === this.state.img;
    let notNumber = isNaN(this.state.price);
    return isNull || notNumber;
  }

  handleClick(e) {
    e.preventDefault();
    const product = {
      name: this.state.name,
      price: this.state.price,
      unit: this.state.unit,
      imgUrl: this.state.img
    };
    this.props.addProduct(product);
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  addProduct
}, dispatch);

export default connect(null, mapDispatchToProps)(AddProduct);
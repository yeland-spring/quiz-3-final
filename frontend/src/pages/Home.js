import React from "react"
import Header from "../components/Header";
import Product from "../components/Product";
import Footer from "../components/Footer";
import {addProduct, getProducts} from "../actions/productActions";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

class Home extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  componentDidMount() {
    this.props.getProducts();
  }

  render() {
    let products = this.props.products;
    console.log(products);
    return (
      <div>
        <Header/>
        <div>
          {products.map((product, index) => <Product product={product} key={index}/>)}
        </div>
        <Footer/>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  products: state.productReducer.products
});

const mapDispatchToProps = dispatch => bindActionCreators({
  addProduct, getProducts
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);
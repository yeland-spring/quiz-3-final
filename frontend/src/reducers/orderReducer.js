const initState = {
  orderProducts: [{item: {}}]
};

export default (state = initState, action) => {
  switch (action.type) {
    case "ADD_ORDER_PRODUCT":
      return {
        ...state,
        isDisabled: action.isDisabled
      };
    case "SET_IS_DISABLED":
      return {
        ...state,
        isDisabled: action.isDisabled
      };
    case "GET_ORDER_PRODUCTS":
      return {
        ...state,
        orderProducts: action.orderProducts
      };
    case "DELETE_ORDER_PRODUCTS":
      return {
        ...state
      };
    default:
      return state;
  }
};
import {fetchToCreateProduct, fetchToGetProducts} from "../resources/fetch";

export const addProduct = (product) => (dispatch) => {
  fetchToCreateProduct(product)
    .then(() => {
      dispatch({
        type: 'ADD_PRODUCT'
      });
    });
};

export const getProducts = () => (dispatch) => {
  fetchToGetProducts()
    .then(result => {
      dispatch({
        type: 'GET_PRODUCTS',
        products: result
      });
    })
};


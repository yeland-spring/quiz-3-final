import {fetchToCreateOrderProduct, fetchToDeleteOrderProduct, fetchToGetOrderProducts} from "../resources/fetch";

export const addOrderProduct = (orderProduct) => (dispatch) => {
  fetchToCreateOrderProduct(orderProduct)
    .then(() => {
      dispatch({
        type: 'ADD_ORDER_PRODUCT',
        isDisabled: false
      })
    })
};

export const setIsDisabled = () => (dispatch) => {
  return dispatch({
    type: 'SET_IS_DISABLED',
    isDisabled: true
  })
};

export const getOrderProducts = () => (dispatch) => {
  fetchToGetOrderProducts()
    .then(result => {
      dispatch({
        type: 'GET_ORDER_PRODUCTS',
        orderProducts: result
      })
    })
};

export const deleteOrderProduct = (id) => (dispatch) => {
  fetchToDeleteOrderProduct(id)
    .then(() => {
      dispatch({
        type: 'DELETE_ORDER_PRODUCTS'
      });
    })
};
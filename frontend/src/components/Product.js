import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {addOrderProduct, setIsDisabled} from "../actions/orderActions";

class Product extends React.Component{
  constructor(props, context) {
    super(props, context);
    this.handleClickAdd = this.handleClickAdd.bind(this);
  }

  render() {
    return (
      <div>
        <img src={this.props.product.imgUrl} alt=""/>
        <h1>{this.props.product.name}</h1>
        <p>{this.props.product.price}元/{this.props.product.unit}</p>
        <button disabled={this.props.isDisabled} onClick={this.handleClickAdd}>+</button>
      </div>
    )
  }

  handleClickAdd() {
    const orderProduct = {
      name: this.props.product.name,
      quantity: 1
    };
    this.props.setIsDisabled();
    this.props.addOrderProduct(orderProduct);
  }
}

const mapStateToProps = state => ({
  isDisabled: state.productReducer.isDisabled
});

const mapDispatchToProps = dispatch => bindActionCreators({
  addOrderProduct, setIsDisabled
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Product);
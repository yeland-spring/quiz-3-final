import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import {deleteOrderProduct, getOrderProducts} from "../actions/orderActions";
import {connect} from "react-redux";

class OrderProduct extends Component {
  constructor(props, context) {
    super(props, context);
    this.handleClickDelete = this.handleClickDelete.bind(this);
  }

  render() {
    return (
      <tr>
        <td>{this.props.product.item.name}</td>
        <td>{this.props.product.item.price}</td>
        <td>{this.props.product.quantity}</td>
        <td>{this.props.product.item.unit}</td>
        <td>
          <button onClick={this.handleClickDelete}>删除</button>
        </td>
      </tr>
    );
  }

  handleClickDelete() {
    this.props.deleteOrderProduct(this.props.product.id);
  }
}

const mapStateToProps = state => ({
  orderProducts: state.orderReducer.orderProducts
});

const mapDispatchToProps = dispatch => bindActionCreators({
  deleteOrderProduct, getOrderProducts
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(OrderProduct);
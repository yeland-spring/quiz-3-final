import React from "react"

const Footer = () => {
  return (
    <footer>
      TW Mail &copy;2018 Created by ForCheng
    </footer>
  )
};

export default Footer;
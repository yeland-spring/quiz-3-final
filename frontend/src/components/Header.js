import React from "react";
import {NavLink} from "react-router-dom";

const Header = () => {
  return (
    <nav>
      <ul>
        <li>
          <NavLink to="/" exact activeClassName="activeLink">商城</NavLink>
        </li>
        <li>
          <NavLink to="/order" activeClassName="activeLink">订单</NavLink>
        </li>
        <li>
          <NavLink to="/products/add" activeClassName="activeLink">+ 添加商品</NavLink>
        </li>
      </ul>
    </nav>
  );
};

export default Header;
import React from "react"

class Input extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.handleChange = this.handleChange.bind(this);
  }

  render() {
    return (
      <div>
        <p>*{this.props.name}</p>
        <input onChange={this.handleChange}/>
      </div>
    );

  }

  handleChange(e) {
    this.props.getInput(e.target.value);
  }
}

export default Input;
import React, {Component} from 'react';
import './App.less';
import Home from "./pages/Home";
import Order from "./pages/Order";
import AddProduct from "./pages/AddProduct";
import {BrowserRouter as Router, Switch} from "react-router-dom";
import {Route} from "react-router-dom";

class App extends Component {
  render() {
    return (
      <div className='App'>
        <Router>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/order" component={Order}/>
            <Route path="/products/add" component={AddProduct}/>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
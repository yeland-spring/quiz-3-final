export function fetchToCreateProduct(product) {
  return fetch(`http://localhost:8080/api/items`,
    {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(product)
    })
    .then((response => {
      if (response.status === 400) {
        alert("商品名称已存在，请输入新的商品名称");
      }
    }));
}

export function fetchToGetProducts() {
  return fetch(`http://localhost:8080/api/items`)
    .then(response => response.json());
}

export function fetchToCreateOrderProduct(orderProduct) {
  return fetch(`http://localhost:8080/api/order-items`,
    {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(orderProduct)
    });
}

export function fetchToGetOrderProducts() {
  return fetch(`http://localhost:8080/api/order-items`)
    .then(response => response.json());
}

export function fetchToDeleteOrderProduct(id) {
  return fetch(`http://localhost:8080/api/order-items/${id}`, {method: "delete"});
}